We repack the upstream distribution file for several reasons:

* Upstream ships two Windows executable files, resulting in a
  source-contains-prebuilt-windows-binary Lintian warning.
* Upstream ships a manual for a retired Mathematica script in pdf format.
  The source code is not included, so we remove it.
  If you would like to read the manual, it is available online at
  https://github.com/BenjaminJurke/cohomCalg/blob/master/old/cohomcalg-script-v004/manual.pdf
* Upstream ships the source for the polylib library.  We remove it and use
  the Debian polylib package instead.
* The cohomCalg logo incorporates a light bulb image with a non-DFSG license.
  We remove the two image files containing this logo from the source and
  references to them from the README and the manual.

Note that this repacking is performed automatically by uscan.

 -- Doug Torrance <dtorrance@piedmont.edu>, Sat, 14 Jul 2018 09:10:32 -0400
